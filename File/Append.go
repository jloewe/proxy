package file

import (
	"os"
	"strings"
)

func Append(path string, text string) {
	if !strings.HasSuffix(text, "\n") {
		text += "\n"
	}

	if !Exists(path) {
		f, err := os.Create(path)
		if err != nil {
			panic(err)
		}
		defer f.Close()
	}

	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	if _, err = f.WriteString(text); err != nil {
		panic(err)
	}
}
