package file

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

func KeepMatchingLinesRegex(path string, pattern *regexp.Regexp) {
	linesRegex(path, pattern, true)
}

func RemoveMatchingLinesRegex(path string, pattern *regexp.Regexp) {
	linesRegex(path, pattern, false)
}

func linesRegex(path string, pattern *regexp.Regexp, keepMatchingLines bool)  {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	builder := strings.Builder{}

	for scanner.Scan() {
		bytes := scanner.Bytes()
		if keepMatchingLines == pattern.Match(bytes) {
			builder.WriteString(fmt.Sprintf("%s\n", bytes))
		}
	}

	if err = ioutil.WriteFile(path, []byte(builder.String()), 0644); err != nil {
		panic(err)
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}
}
