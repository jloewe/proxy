package file

import (
	"io/ioutil"
	"regexp"
)

func ContainsRegex(path string, pattern *regexp.Regexp) bool {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	return pattern.Match(b)
}
