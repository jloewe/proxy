package proxy

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"gitlab.com/jloewe/proxy/Modules"
	"os"
)

type Manager struct {
	Modules []proxy_module.Module
	Log     distillog.Logger
}

const hr string = "----------------------"

func NewManager(log distillog.Logger) Manager {
	if log == nil {
		log = distillog.NewStdoutLogger("Proxy")
	}

	if !IsRoot(log) {
		log.Errorln("Please run the script with root permissions")
		os.Exit(1)
	}

	modules := []proxy_module.Module{
		proxy_module.EnvironmentModule{Log: log},
		proxy_module.SudoersModule{Log: log},
		proxy_module.AptModule{Log: log},
		proxy_module.BashModule{Log: log},
		proxy_module.ZshModule{Log: log},
		proxy_module.GitModule{Log: log},
		proxy_module.NpmModule{Log: log},
		proxy_module.YarnModule{Log: log},
		proxy_module.MacModule{Log: log},
	}

	pm := Manager{Modules: modules, Log: log}
	return pm
}

func printStartMessage(log distillog.Logger) {
	log.Infoln("Proxy Script by jloewe")
	log.Infoln(hr)
}

func printEndMessage(log distillog.Logger) {
	log.Infoln(hr)
	log.Infoln("Script execution finished")
}

func (pm Manager) Set(p datatype.Proxy) {
	printStartMessage(pm.Log)

	if len(p.Host) <= 0 {
		pm.Log.Errorln("Host is empty")
		return
	}

	if len(p.Bypass) <= 0 {
		pm.Log.Warningln("Bypass is empty")
	}

	for _, module := range pm.Modules {
		if module.IsAvailable() {
			module.Set(p)
		}
	}

	printEndMessage(pm.Log)
}

func (pm Manager) Unset() {
	printStartMessage(pm.Log)

	for _, module := range pm.Modules {
		if module.IsAvailable() {
			module.Unset()
		}
	}

	printEndMessage(pm.Log)
}
