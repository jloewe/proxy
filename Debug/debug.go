package main

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy"
	"gitlab.com/jloewe/proxy/DataType"
)

func main() {
	manager := proxy.NewManager(distillog.NewStdoutLogger("Proxy"))
	manager.Set(datatype.Proxy{Host: "webproxy.bs.ptb.de", Port: 8080, Bypass: []string{"itgit.bs.ptb.de"}})
	// manager.Set(datatype.Proxy{Host:"192.168.49.1", Port:8282, Bypass:nil})
	// manager.Unset();
}
