package proxy

import (
	"github.com/amoghe/distillog"
	"os/exec"
	"strconv"
)

// https://www.socketloop.com/tutorials/golang-force-your-program-to-run-with-root-permissions
func IsRoot(log distillog.Logger) bool {
	output, err := exec.Command("id", "-u").CombinedOutput()

	if err != nil {
		log.Errorln(err)
	}

	// 0 = root, 501 = non-root user
	i, err := strconv.Atoi(string(output[:len(output)-1]))

	if err != nil {
		log.Errorln(err)
	}

	return i == 0
}
