package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"gitlab.com/jloewe/proxy/File"
	"os/exec"
	"regexp"
)

type SudoersModule struct {
	Log distillog.Logger
}

func (s SudoersModule) IsAvailable() bool {
	return true
}

func (s SudoersModule) ExecuteCommand(name string, args ...string) {
	cmd := exec.Command(name, args...)
	if err := cmd.Run(); err != nil {
		s.Log.Errorln(err.Error())
	}
}

const sudoersPath string = "/etc/sudoers"

func (s SudoersModule) Set(p datatype.Proxy) {
	s.Log.Infoln("Adding proxy vars to env_keep => /etc/sudoers")

	regex, _ := regexp.Compile("(http_proxy|https_proxy|ftp_proxy|no_proxy)")

	if file.Exists(sudoersPath) &&
		!file.ContainsRegex(sudoersPath, regex) {
		file.Append(sudoersPath, "Defaults env_keep += \"http_proxy https_proxy ftp_proxy no_proxy\"\n")
	}
}

func (s SudoersModule) Unset() {
}
