package proxy_module

import (
	"gitlab.com/jloewe/proxy/DataType"
)

type Module interface {
	IsAvailable() bool
	Set(p datatype.Proxy)
	Unset()
}
