package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"gitlab.com/jloewe/proxy/File"
	"os/exec"
	"regexp"
)

type AptModule struct {
	Log distillog.Logger
}

func (apt AptModule) IsAvailable() bool {
	return IsCommandAvailable("apt-get")
}

func (apt AptModule) ExecuteCommand(name string, args ...string) {
	cmd := exec.Command(name, args...)
	if err := cmd.Run(); err != nil {
		apt.Log.Errorln(err.Error())
	}
}

const aptFolder string = "/etc/apt"
const aptPath = aptFolder + "/apt.conf"

func (apt AptModule) Set(p datatype.Proxy) {
	apt.Log.Infoln("Setting Proxy for apt")

	if !file.Exists(aptFolder) {
		apt.Log.Warningln("No config folder for apt found")
		return
	}

	regex, _ := regexp.Compile("(http::proxy|https::proxy|ftp::proxy)")

	if file.Exists(aptPath) && file.ContainsRegex(aptPath, regex) {
		file.RemoveMatchingLinesRegex(aptPath, regex)
	}

	proxy := p.GetHttpHostAndPort()

	file.Append(aptPath, "Acquire::http::proxy \""+proxy+"\";"+"\n"+
		"Acquire::https::proxy \""+proxy+"\";"+"\n"+
		"Acquire::ftp::proxy \""+proxy+"\";"+"\n")
}

func (apt AptModule) Unset() {
	apt.Log.Infoln("Unsetting Proxy for apt")

	if !file.Exists(aptPath) {
		apt.Log.Warningln("No config file for apt found")
		return
	}

	regex, _ := regexp.Compile("(http::proxy|https::proxy|ftp::proxy)")
	file.RemoveMatchingLinesRegex(aptPath, regex)
}
