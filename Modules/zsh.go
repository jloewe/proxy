package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"gitlab.com/jloewe/proxy/File"
	"regexp"
)

type ZshModule struct {
	Log distillog.Logger
}

func (zsh ZshModule) IsAvailable() bool {
	return IsCommandAvailable("zsh")
}

const zshPath string = "/etc/zshrc"

func (zsh ZshModule) Set(p datatype.Proxy) {
	zsh.Log.Infoln("Setting Proxy for zsh")

	regex, _ := regexp.Compile("(http_proxy|https_proxy|ftp_proxy|no_proxy)")

	if file.Exists(zshPath) &&
		file.ContainsRegex(zshPath, regex) {
		file.RemoveMatchingLinesRegex(zshPath, regex)
	}

	file.Append(zshPath, p.GetBashExports())
}

func (zsh ZshModule) Unset() {
	zsh.Log.Infoln("Unsetting Proxy for zsh")
	regex, _ := regexp.Compile("(http_proxy|https_proxy|ftp_proxy|no_proxy)")
	file.RemoveMatchingLinesRegex(zshPath, regex)
}
