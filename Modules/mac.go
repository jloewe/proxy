package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"os/exec"
	"strconv"
)

type MacModule struct {
	Log distillog.Logger
}

func (mac MacModule) IsAvailable() bool {
	return IsCommandAvailable("networksetup")
}

func (mac MacModule) ExecuteCommand(name string, args ...string) {
	cmd := exec.Command(name, args...)
	if err := cmd.Run(); err != nil {
		mac.Log.Errorln(err.Error())
	}
}

var interfaces = [...]string{"wi-fi", "Ethernet"}

func (mac MacModule) Set(p datatype.Proxy) {
	mac.Log.Infoln("Setting Proxy for Mac")

	for i := 0; i < len(interfaces); i++ {
		var inf = interfaces[i]
		mac.ExecuteCommand("networksetup", "-setwebproxystate", inf, "on")
		mac.ExecuteCommand("networksetup", "-setwebproxy", inf, p.Host, strconv.Itoa(p.Port), "off")

		mac.ExecuteCommand("networksetup", "-setsecurewebproxystate", inf, "on")
		mac.ExecuteCommand("networksetup", "-setsecurewebproxy", inf, p.Host, strconv.Itoa(p.Port), "off")

		mac.ExecuteCommand("networksetup", "-setsecurewebproxystate", inf, "on")
		mac.ExecuteCommand("networksetup", "-setftpproxy", inf, p.Host, strconv.Itoa(p.Port), "off")
	}
}

func (mac MacModule) Unset() {
	mac.Log.Infoln("Unsetting Proxy for Mac")

	for i := 0; i < len(interfaces); i++ {
		var inf = interfaces[i]

		mac.ExecuteCommand("networksetup", "-setwebproxystate", inf, "off")
		mac.ExecuteCommand("networksetup", "-setsecurewebproxystate", inf, "off")
		mac.ExecuteCommand("networksetup", "-setftpproxystate", inf, "off")
	}
}
