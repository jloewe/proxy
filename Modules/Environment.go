package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"gitlab.com/jloewe/proxy/File"
	"os/exec"
	"regexp"
)

type EnvironmentModule struct {
	Log distillog.Logger
}

func (env EnvironmentModule) IsAvailable() bool {
	return true
}

func (env EnvironmentModule) ExecuteCommand(name string, args ...string) {
	cmd := exec.Command(name, args...)
	if err := cmd.Run(); err != nil {
		env.Log.Errorln(err.Error())
	}
}

const envPath string = "/etc/environment"

func (env EnvironmentModule) Set(p datatype.Proxy) {
	env.Log.Infoln("Setting Proxy for env")

	regex, _ := regexp.Compile("(http_proxy|https_proxy|ftp_proxy|no_proxy)")

	if file.Exists(envPath) &&
		file.ContainsRegex(envPath, regex) {
		file.RemoveMatchingLinesRegex(envPath, regex)
	}

	file.Append(envPath, p.GetEnvironmentVars())
}

func (env EnvironmentModule) Unset() {
	env.Log.Infoln("Unsetting Proxy for env")
	regex, _ := regexp.Compile("(http_proxy|https_proxy|ftp_proxy|no_proxy)")
	file.RemoveMatchingLinesRegex(envPath, regex)
}
