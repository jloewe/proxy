package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"gitlab.com/jloewe/proxy/File"
	"os/exec"
	"regexp"
)

type BashModule struct {
	Log distillog.Logger
}

func (bash BashModule) IsAvailable() bool {
	return true
}

func (bash BashModule) ExecuteCommand(name string, args ...string) {
	cmd := exec.Command(name, args...)
	if err := cmd.Run(); err != nil {
		bash.Log.Errorln(err.Error())
	}
}

var bashPaths = [...]string{"/etc/bashrc", "/etc/bash.bashrc"}

func (bash BashModule) Set(p datatype.Proxy) {
	bash.Log.Infoln("Setting Proxy for bash")

	regex, _ := regexp.Compile("(http_proxy|https_proxy|ftp_proxy|no_proxy)")

	for i := 0; i < len(bashPaths); i++ {
		var bashPath = bashPaths[i]
		if file.Exists(bashPath) &&
			file.ContainsRegex(bashPath, regex) {
			file.RemoveMatchingLinesRegex(bashPath, regex)
		}

		file.Append(bashPath, p.GetBashExports())
	}
}

func (bash BashModule) Unset() {
	bash.Log.Infoln("Unsetting Proxy for bash")
	regex, _ := regexp.Compile("(http_proxy|https_proxy|ftp_proxy|no_proxy)")
	for i := 0; i < len(bashPaths); i++ {
		var bashPath = bashPaths[i]
		file.RemoveMatchingLinesRegex(bashPath, regex)
	}
}
