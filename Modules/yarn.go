package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"os/exec"
)

type YarnModule struct {
	Log distillog.Logger
}

func (yarn YarnModule) IsAvailable() bool {
	return IsCommandAvailable("yarn")
}

func (yarn YarnModule) Set(p datatype.Proxy) {
	yarn.Log.Infoln("Setting Proxy for Yarn")

	cmd := exec.Command("yarn", "config", "set", "proxy", p.GetHostAndPort())
	if err := cmd.Run(); err != nil {
		yarn.Log.Errorln(err.Error())
	}

	cmd2 := exec.Command("yarn", "config", "set", "https-proxy", p.GetHostAndPort())
	if err := cmd2.Run(); err != nil {
		yarn.Log.Errorln(err.Error())
	}
}

func (yarn YarnModule) Unset() {
	yarn.Log.Infoln("Unsetting Proxy for Yarn")

	cmd := exec.Command("yarn", "config", "delete", "proxy")
	if err := cmd.Run(); err != nil {
		yarn.Log.Errorln(err.Error())
	}

	cmd2 := exec.Command("yarn", "config", "delete", "https-proxy")
	if err := cmd2.Run(); err != nil {
		yarn.Log.Errorln(err.Error())
	}
}
