package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"os/exec"
)

type NpmModule struct {
	Log distillog.Logger
}

func (npm NpmModule) IsAvailable() bool {
	return IsCommandAvailable("npm")
}

func (npm NpmModule) Set(p datatype.Proxy) {
	npm.Log.Infoln("Setting Proxy for NPM")

	cmd := exec.Command("npm", "config", "set", "proxy", p.GetHostAndPort())
	if err := cmd.Run(); err != nil {
		npm.Log.Errorln(err.Error())
	}

	cmd2 := exec.Command("npm", "config", "set", "https-proxy", p.GetHostAndPort())
	if err := cmd2.Run(); err != nil {
		npm.Log.Errorln(err.Error())
	}

}

func (npm NpmModule) Unset() {
	npm.Log.Infoln("Unsetting Proxy for NPM")

	cmd := exec.Command("npm", "config", "delete", "proxy")
	if err := cmd.Run(); err != nil {
		npm.Log.Errorln(err.Error())
	}

	cmd2 := exec.Command("npm", "config", "delete", "https-proxy")
	if err := cmd2.Run(); err != nil {
		npm.Log.Errorln(err.Error())
	}
}
