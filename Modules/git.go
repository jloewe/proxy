package proxy_module

import (
	"github.com/amoghe/distillog"
	"gitlab.com/jloewe/proxy/DataType"
	"gitlab.com/jloewe/proxy/File"
	"os"
	"os/exec"
	"regexp"
)

type GitModule struct {
	Log distillog.Logger
}

func (git GitModule) IsAvailable() bool {
	return IsCommandAvailable("git")
}

func (git GitModule) Set(p datatype.Proxy) {
	git.Log.Infoln("Setting Proxy for Git")

	cmd := exec.Command("git", "config", "--global", "http.proxy", p.GetHostAndPort())
	if err := cmd.Run(); err != nil {
		git.Log.Errorln(err.Error())
	}

	cmd2 := exec.Command("git", "config", "--global", "https.proxy", p.GetHostAndPort())
	if err := cmd2.Run(); err != nil {
		git.Log.Errorln(err.Error())
	}

	for _, bypass := range p.GetSanitizedBypass() {
		cmd3 := exec.Command("git", "config", "--global", "http.https://"+bypass+".proxy", "")
		if err := cmd3.Run(); err != nil {
			git.Log.Errorln(err.Error())
		}
	}
}

func (git GitModule) Unset() {
	git.Log.Infoln("Unsetting Proxy for Git")

	cmd := exec.Command("git", "config", "--global", "--remove-section", "http")
	if err := cmd.Run(); err != nil {
		git.Log.Errorln(err.Error())
	}

	cmd2 := exec.Command("git", "config", "--global", "--remove-section", "https")
	if err := cmd2.Run(); err != nil {
		git.Log.Errorln(err.Error())
	}

	// remove proxy bypass
	gitPath := os.Getenv("HOME") + "/.gitconfig"
	regex, _ := regexp.Compile("(\\[http(s)?( .*)?]|proxy.*=.*)")
	if file.Exists(gitPath) {
		file.RemoveMatchingLinesRegex(gitPath, regex)
	}
}
