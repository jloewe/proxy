package datatype

import (
	"fmt"
	"strconv"
	"strings"
)

type Proxy struct {
	Host   string
	Port   int
	Bypass []string
}

func (p Proxy) GetSanitizedBypass() []string {
	result := []string{}
	for _, bypass := range p.Bypass {
		temp := strings.TrimLeft(bypass, "http://")
		temp = strings.TrimLeft(bypass, "https://")
		result = append(result, temp)
	}
	return result
}

func (p Proxy) GetHostAndPort() string {
	return fmt.Sprintf("%s:%s", p.Host, strconv.Itoa(p.Port))
}

func (p Proxy) GetHttpHostAndPort() string {
	return fmt.Sprintf("http://%s/", p.GetHostAndPort())
}

func (p Proxy) GetEnvironmentVars() string {
	hostAndPort := p.GetHttpHostAndPort()
	envVars := "http_proxy=" + hostAndPort + "\n" +
		"https_proxy=" + hostAndPort + "\n" +
		"ftp_proxy=" + hostAndPort + "\n"

	if len(p.Bypass) > 0 {
		envVars += "no_proxy=" + strings.Join(p.Bypass, ",") + "\n"
	}

	return envVars
}

func (p Proxy) GetBashExports() string {
	hostAndPort := p.GetHttpHostAndPort()
	bashExports := "export http_proxy=" + hostAndPort + "\n" +
		"export https_proxy=" + hostAndPort + "\n" +
		"export ftp_proxy=" + hostAndPort + "\n"

	if len(p.Bypass) > 0 {
		bashExports += "export no_proxy=" + strings.Join(p.Bypass, ",") + "\n"
	}

	return bashExports
}
